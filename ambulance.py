# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement
from decimal import Decimal
from dateutil import tz
from datetime import datetime, time, timedelta, date
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.report import Report
from trytond import backend
from trytond.pyson import Eval, If, In, Get, Id
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.wizard import Wizard, StateView, Button, StateAction, StateTransition, StateReport
from sql import Literal, Join

__all__ = []

STATES = {
    'readonly': (Eval('state') != 'draft'),
}


from_zone = tz.gettz('UTC')
to_zone = tz.gettz('America/Bogota')

_ZERO = Decimal('0.0')


class Ambulance(Workflow, ModelSQL, ModelView):
    'Rental Service'
    __name__ = 'gnuhealth.ambulance'
    _rec_name = 'vehicle'
    company = fields.Many2One('company.company', 'Company', required=True,
            states=STATES, domain=[('id', If(In('company',
            Eval('context', {})), '=', '!='), Get(Eval('context', {}),
            'company', 0)), ])
    ambulance = fields.Many2One('maintenance.equipment', 'Ambulance',
            required=True, states=STATES, domain=[
                ('type_device', '=', 'vehicle'),
            ])


    @classmethod
    def __setup__(cls):
        super(Ambulance, cls).__setup__()
        cls._order = [
            ('schedule_date', 'DESC'),
            ('vehicle', 'DESC'),
        ]

        cls._error_messages.update({
                'invalid_dates': ('Error in Date Start Service!'),
                'invalid_state': ('State of Service invalid, for generate sale '
                'this service should be in draft state!'),
                'equipment_duplicate': ('This Equipment is used in another service'),
        })

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('number',) + tuple(clause[1:]),
            ('equipment',) + tuple(clause[1:]),
        ]

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['number'] = None
        default['state'] = 'draft'
        new_records = []
        for record in records:
            new_record, = super(Service, cls).copy(
                    [record], default=default
                    )
            new_records.append(new_record)
        return new_records

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    @staticmethod
    def default_balance():
        return 0

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_currency():
        Company = Pool().get('company.company')
        company = Transaction().context.get('company')
        if company:
            return Company(company).currency.id

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('waiting')
    def waiting(cls, records):
        for service in records:
            duplicates = cls.search_read([
                ('equipment.id', '=', service.equipment.id),
                ('state', 'in', ['done', 'cancel', 'draft']),
            ], fields_names=['equipment'])
            if len(duplicates) >= 2:
                cls.raise_user_error('equipment_duplicate!')
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirmed(cls, records):
        cls.set_odometer_start(records)
        cls.set_number(records)
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('pickup')
    def pickup(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        cls.update_odomenter_equipment(records)
        for record in records:
            pass

    @classmethod
    def validate(cls, services):
        for service in services:
            service.check_dates()


    def check_dates(self):
        today = datetime.now()
        if self.start_date_service:
            if self.start_date_service > today:
                self.raise_user_error('invalid_dates')
            if self.date_return:
                if self.date_return < self.start_date_service:
                    self.raise_user_error('invalid_dates')

    @classmethod
    def set_number(cls, requests):
        '''
        Fill the number field with the service sequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('rental.configuration')
        config = Config(1)

        for request in requests:
            if request.number:
                continue
            if not config.rental_service_sequence:
                continue
            number = Sequence.get_id(config.rental_service_sequence.id)
            cls.write([request], {'number': number})

    @classmethod
    def set_odometer_start(cls, requests):
        pool = Pool()
        Equipment = pool.get('maintenance.equipment')
        for request in requests:
            equipments = Equipment.search([
            ('id', '=', request.equipment.id)
            ])
            if equipments:
                odometer = request.equipment.odometer
            cls.write([request], {'odometer_start': odometer})

    @fields.depends('unit_price', 'service_product', 'days_service')
    def on_change_service_product(self, name=None):
        if self.service_product and self.days_service:
            self.unit_price = self.service_product.list_price * self.days_service
        else:
            self.unit_price = None


    @fields.depends('days_service', 'start_date_service')
    def on_change_with_end_date_service(self, name=None):
        if self.start_date_service and self.days_service:
            end_date_service = self.start_date_service + timedelta(days=int(self.days_service))
            return end_date_service

    @fields.depends('date_return', 'end_date_service')
    def on_change_with_early_return(self, name=None):
        if self.date_return and self.end_date_service:
            days = (self.end_date_service - self.date_return ).days
            if int(days) >0:
                return True

    @fields.depends('equipment')
    def get_check_list(self, name=None):
        pool = Pool()
        Check_list = pool.get('maintenance.check_list_equipment')
        if self.equipment:
            check_list = Check_list.search([
            ('equipment', '=', self.equipment.id),
            ])
            ids = []
            for c in check_list:
                ids.append(c.id)
            return ids
        else:
            return None

    @fields.depends('payments', 'unit_price', 'early_return')
    def get_balance(self, name=None):
        pool = Pool()
        Configuration = pool.get('rental.configuration')
        config = Configuration.search([])
        amounts = []
        amount_to_pay = 0
        if self.unit_price:
            amount_to_pay += self.unit_price
        if self.payments:
            for payment in self.payments:
                amounts.append(payment.amount_to_pay)
            amount_to_pay -= sum(amounts)
        if self.early_return:
            amount_to_pay += config[0].value_early_return
        return amount_to_pay


    def get_days_expiration(self, name):
        today = datetime.now()
        if self.end_date_service and today < self.end_date_service:
            days_res = self.end_date_service - today
            d = timedelta(days=1)
            days_exp = days_res.days
            return str(days_exp)
        else:
            return '0'

    def update_odomenter_equipment(requests):
        pool = Pool()
        Equipment = pool.get('maintenance.equipment')
        for request in requests:
            if not request.odometer_end:
                continue
            equipments = Equipment.search([
                ('id', '=', request.equipment.id),
            ])
            super(Equipment, Equipment).write([equipments[0]], {'odometer': request.odometer_end})



class RentalIssue(ModelSQL, ModelView):
    "Rental Issue"
    __name__ = "rental.issue"
    name = fields.Char('Name Issue', required=True, select=True)
    rental = fields.Many2One('rental.service', 'Rental',
        ondelete='CASCADE', required=True)

class RentalServiceReport(Report):
    __name__ = 'rental.service.report'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(RentalServiceReport, cls).get_context(records, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        dat = date.today()
        report_context['today'] = dat
        return report_context


class ServiceForceDraft(Wizard):
    'Service Force Draft'
    __name__ = 'rental.service.force_draft'
    start_state = 'force_draft'
    force_draft = StateTransition()

    def transition_force_draft(self):
        cursor = Transaction().connection.cursor()
        ids = Transaction().context['active_ids']
        if ids:
            ids = str(ids[0])
            query = "UPDATE rental_service SET state='draft' WHERE id=%s"
            cursor.execute(query % ids)
        return 'end'


class CreateSaleFromService(Wizard):
    'Create Sale From Services'
    __name__ = 'rental.service.create_sale'
    """
    this is the wizard that allows create a sale from services executed.
    """
    start_state = 'create_sale'
    create_sale = StateTransition()

    def transition_create_sale(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        SaleLine = pool.get('sale.line')
        RentalService = pool.get('rental.service')
        ids = Transaction().context['active_ids']
        company_id = Transaction().context['company']
        Party = pool.get('party.party')
        if not ids:
            return 'end'

        services = RentalService.search([
            ('id', 'in', ids),
            ('state', '=', 'done'),
            ('sale', '=', None)
        ])
        if not services:
            self.raise_user_error('invalid_state')

        parties = [s.party for s in services if s.party]
        if not parties:
            return 'end'
        parties = list(set(parties))
        today = date.today()

        party_sale = {}
        for party in parties:

            if party.id not in party_sale.keys():
                sale = Sale(
                    company=company_id,
                    party=party.id,
                    price_list=None,
                    sale_date=today,
                    state='draft',
                    # origin=self,
                    invoice_address=Party.address_get(party, type='invoice'),
                    shipment_address=Party.address_get(party, type='delivery'),
                    reference='Service No.'+services[0].number,
                    description='Sale for Rental Service',
                )
                sale.save()
                party_sale[party.id] = sale

        for service in services:
            if not service.party:
                continue

            sale = party_sale.get(service.party.id)
            new_line = {
                'sale': sale.id,
                'type': 'line',
                'unit': service.service_product.template.default_uom.id,
                'quantity': 1,
                'unit_price': service.unit_price,
                'product': service.service_product.id,
                'description': service.service_product.rec_name,
            }
            SaleLine.create([new_line])
            Sale.quote([sale])
            Sale.confirm([sale])
            Sale.process([sale])
            print(sale, 'hola')
            sales = [sale.id]
            service.sales= sales
            service.save()
        return 'end'

class RentalSale(ModelSQL):
    'Rental - Sale'
    __name__ = 'rental.service-sale.sale'
    _table = 'rental_sales_rel'
    rental = fields.Many2One('rental.service', 'Rental',
            ondelete='CASCADE', select=True, required=True)
    sale = fields.Many2One('sale.sale', 'Sale',
            ondelete='RESTRICT', select=True, required=True)

class RentalVoucher(ModelSQL):
    'Rental - Voucher'
    __name__ = 'rental.service-account.voucher'
    _table = 'rental_vouchers_rel'
    rental = fields.Many2One('rental.service', 'Rental',
            ondelete='CASCADE', select=True, required=True)
    voucher = fields.Many2One('account.voucher', 'Voucher',
            domain=[('voucher_type', '=', 'receipt')],
            ondelete='RESTRICT', select=True, required=True)

class EquipmentStateReport(ModelSQL, ModelView):
    'Equipment State Report'
    __name__ = 'rental.equipmentState.report'

    service_product = fields.Many2One('product.product', 'Service')
    equipment = fields.Many2One('maintenance.equipment', 'Equipment')
    start_date_service = fields.DateTime('Start Date Effective')
    end_date_service = fields.DateTime('End Date Contract', readonly=True, depends=['days_service', 'start_date_service'])
    days_expiration = fields.Function(fields.Char('Days To Expiration', readonly=True), 'get_days_expiration')
    days_service = fields.Numeric('Days Service')
    state = fields.Char('State')
    number = fields.Char('Service')

    @classmethod
    def __setup__(cls):
        super(EquipmentStateReport, cls).__setup__()

    @classmethod
    def table_query(cls):
        pool = Pool()
        rental_service = pool.get('rental.service').__table__()

        return rental_service.select(
            rental_service.id,
            rental_service.create_uid,
            rental_service.create_date,
            rental_service.write_uid,
            rental_service.write_date,
            rental_service.service_product,
            rental_service.equipment,
            rental_service.number,
            rental_service.start_date_service,
            rental_service.end_date_service,
            rental_service.days_service,
            rental_service.state,)

    def get_days_expiration(self, name):
        today = datetime.now()
        if self.end_date_service and today < self.end_date_service:
            days_res = self.end_date_service - today
            d = timedelta(days=1)
            days_exp = (days_res / d)
            if str(days_exp)[0] != '0':
                return str(days_exp)[0:1]
            else:
                return '0'
        else:
            return '0'


# class CheckListReturn(ModelSQL, ModelView):
#     'Check List Equipment'
#     __name__ = 'rental.service.check_list_return'
#     element = fields.Char('Element', required=True)
#     quantity = fields.Numeric('Quantity', required=True)
#     checked = fields.Boolean('Checked')
#     notes = fields.Char('Notes')
#     rental = fields.Many2One('rental.service', 'Rental')
#
#     @classmethod
#     def __setup__(cls):
#         super(CheckListReturn, cls).__setup__()
