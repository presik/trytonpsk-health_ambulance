# This file is part of Tryton.  The COPYRIGHT file at the top level of this
#repository contains the full copyright notices and license terms.
from trytond.pool import Pool
import ambulance

def register():
    Pool.register(
        GlossCategory,
        Gloss,
        Invoice,
        module='health_gloss', type_='model')
    Pool.register(
        GlossDetailedReport,
        GlossReport,
        module='health_gloss', type_='report')
