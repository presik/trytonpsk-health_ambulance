# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .service import (Service, RentalServiceReport, ServiceForceDraft, CreateSaleFromService, RentalIssue,
 RentalVoucher, RentalSale, EquipmentStateReport)
from .configuration import Configuration
from .equipment import (CheckListItem, CheckListEquipment, Equipment)


def register():
    Pool.register(
        Configuration,
        Service,
        RentalIssue,
        RentalVoucher,
        RentalSale,
        EquipmentStateReport,
        CheckListEquipment,
        CheckListItem,
        Equipment,
        module='rental', type_='model')
    Pool.register(
        ServiceForceDraft,
        CreateSaleFromService,
        module='rental', type_='wizard')
    Pool.register(
        RentalServiceReport,
        module='rental', type_='report')
