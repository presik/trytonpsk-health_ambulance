# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pyson import Eval

__all__ = ['Configuration']


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Rental Configuration'
    __name__ = 'rental.configuration'
    law_resolution = fields.Char('Law Resolution')
    rental_service_sequence = fields.Many2One('ir.sequence',
            'Rental Service Sequence', domain=[
                ('company', 'in', [Eval('context', {}).get('company', 0),
                        None]),
                ('code', '=', 'rental.service'),
            ], required=True)
    value_early_return = fields.Numeric('Value to Early Return', digits=(16, 2), required=True)
